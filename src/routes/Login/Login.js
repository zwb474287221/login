import React, {Component} from 'react';
import {connect} from 'dva';
import styles from './Login.css';
import Header from '../../components/Common/Header';
import Input from '../../components/Common/Input';
import {Button, Toast} from 'antd-mobile';
import {sha256} from 'js-sha256';

class Login extends Component {



  state = {
    phone: '',
    password: '',
    loading: false,
  };


  _loginClick = () => {

    let sha2526 = sha256(this.state.password);
    let phone = this.state.phone.replace(/\s/g, "");
    let data = {cellphone: phone, password_hash: sha2526};
    this.props.dispatch({
      type: 'userlogin/fetch',
      payload: data,
      callback: (message) => {Toast.info(message, 2);}
    });

  };

  _callback = (data, type) => {
    if (type === "手机号码") {
      this.setState({
        phone: data
      });
    }
    else {
      this.setState({
        password: data
      });
    }
  };


  render() {
    return (
      <div className={styles["root"]}>
        <Header/>
        <div className={styles["flex"]}>&nbsp;</div>
        <div className={styles["body"]}>
          {
            ["手机号码", "密码"].map((item, i) => {
              return (
                <Input key={i} ref={item} title={item} callback={this._callback.bind(this)}/>
              );
            })
          }
          <Button className={[styles.button, styles.backcolor1]} onClick={() => this._loginClick()}>登陆</Button>
          <Button className={styles.button}>忘记密码</Button>
          <Button className={styles.button}>还没账号？点我注册</Button>
        </div>

        <div className={styles["flex"]}>&nbsp;</div>
      </div>
    );
  }
}

Login.propTypes = {};

export default connect(state => ({userlogin: state.userlogin}))(Login);
