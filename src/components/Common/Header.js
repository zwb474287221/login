import React, {Component} from 'react';
import {connect} from 'dva';
import styles from './Header.css';

class Header extends Component {
  render() {
    return (
      <header className={styles.Header}>
        <div className={styles["L-div"]}><span className={styles["L-title"]}>&lt;</span></div>
        <div className={styles["C-div"]}><span className={styles.title}>GAMEFLIX</span></div>
        <div className={styles["R-div"]}></div>
      </header>
    );
  }
}

Header.propTypes = {};

export default connect()(Header);
