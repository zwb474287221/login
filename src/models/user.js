//import request from '../utils/request'
import {topost} from '../services/example';


export default {

  namespace: 'userlogin',

  state: {
    data: "",
    message: ""
  },

  subscriptions: {
    setup({dispatch, history}) {  // eslint-disable-line
    },
  },

  effects: {
    * fetch({payload, callback}, {call, put}) {  // eslint-disable-line
      const data = yield call(topost, 'login', payload);
      yield put({type: 'login', data: data, callback});
    },
  },

  reducers: {
    login(state, action) {
      switch (action.type) {
        case 'userlogin/login':
          if (action.data.but) {
            if (action.data.but_code === 121) {
              let message = "密码错误";
              action.callback(message);
              return {
                ...state,
              };
            }
            else if (action.data.but_code === 101) {
              let message = "用户名不存在";
              action.callback(message);
              return {
                ...state,
              };
            }
          }
          else {
            let message = "登陆成功";
            action.callback(message);
            return {
              ...state,
              message: message,
              data: action.data
            };
          }
          break;
        default:
          return (state);
      }
    },
  },

};
