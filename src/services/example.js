import request from '../utils/request';

export function query() {
  return request('/api/users');
}

export function login() {
  return request('/api/login');
}

export function topost(api, data) {
  let url = 'https://api-dev.gameflix.cn/' + api;
  let options = {
    method: "post",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  };
  let result = request(url, options);
  return result;
}
