import React, {Component} from 'react';
import {connect} from 'dva';
import {InputItem, Toast} from 'antd-mobile';

class Input extends Component {
  state = {
    hasError: false,
    value: '',
  };

  onErrorClick = () => {
    if (this.state.hasError) {
      Toast.info('Please enter 11 digits');
    }
  };

  onChange = (value) => {

    this.setState({
      value,
    });
    this.props.callback(value, this.props.title);
  };

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.value !== nextState.value;
  }

  render() {
    return (
      <div style={{marginLeft: '0.90909rem', marginRight: '0.90909rem'}}>
        <InputItem
          type={this.props.title === "手机号码" ? "phone" : "password"}
          placeholder={"请输入" + this.props.title}
          error={this.state.hasError}
          onErrorClick={this.onErrorClick}
          onChange={this.onChange}
          value={this.state.value}
        >{this.props.title}</InputItem>
      </div>
    );
  }
}

Input.propTypes = {};

export default connect()(Input);
